export const environment = {
  production: false,
  jwtTokenKey: 'balenc-7362',
  apiUrl: 'localhost',
  cookieName: 'reflaunt-balenciaga',
  balenciagaEmail: 'balenciaga@reflaunt.com',
  clientKey: 'ck_2835e341c3bbd3fd8fd97c8f9d8a65'
};
