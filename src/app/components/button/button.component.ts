import { Component, OnInit } from '@angular/core';
import hasIn from 'lodash/hasIn';
import { IframeService } from '../../common/services/iframe.service';
import { Store } from '../../store/store.module';
import { AppInjector } from '../../app-injector';
import { SMART_BUTTON_OPEN_APPLICATION_MESSAGE_TYPE } from '../../iframe-message.const';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-button',
  template: ` <h1>Button Component</h1> `
})
export class ButtonComponent {
  private store;
  public translate;

  constructor(public iframeService: IframeService, public activatedRoute: ActivatedRoute) {
    this.store = AppInjector.get(Store).getInstance();
    this.translate = AppInjector.get(TranslateService);
  }

  get payload() {
    return this.store.getState().Root;
  }

  get priceEstimated() {
    return this.store.getState().Root.priceEstimated;
  }

  buttonOnInit(elId) {
    const button = document.getElementById(elId);
    if (button) {
      this.openApplication(button);
    }
  }

  openApplication(button) {
    if (this.isOpenApplication()) {
      if (this.isOpenSpecificScreen()) {
        this.sendOpenApplicationMessageToParent();
      }
      button.click();
    }
  }

  isOpenApplication() {
    const { queryParams } = this.activatedRoute.snapshot;
    const product = this.payload.product.item;
    if (hasIn(queryParams, 'open_smart_button_product_id') && hasIn(queryParams, 'open_smart_button_order_id')) {
      if (
        Number(queryParams.open_smart_button_product_id) === Number(product.source_product_id) &&
        Number(queryParams.open_smart_button_order_id) === Number(product.source_order_id)
      ) {
        return true;
      }
    }
    return false;
  }

  isOpenSpecificScreen() {
    const { queryParams } = this.activatedRoute.snapshot;
    if (hasIn(queryParams, 'is_tncs')) {
      return true;
    }
    if (hasIn(queryParams, 'open_smart_button_screen')) {
      return true;
    }
    return false;
  }

  sendOpenApplicationMessageToParent() {
    this.iframeService.sendToParent({ type: SMART_BUTTON_OPEN_APPLICATION_MESSAGE_TYPE, payload: this.payload });
  }
}
