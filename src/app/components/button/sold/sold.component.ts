import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sold',
  templateUrl: './sold.component.html',
  styleUrls: ['./sold.component.scss']
})
export class SoldComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('sold-open-button');
  }

  onClick() {
    this.sendOpenApplicationMessageToParent();
  }
}
