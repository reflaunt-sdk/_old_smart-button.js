import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonRoutingModule } from './button-routing.module';
import { ResellComponent } from './resell/resell.component';
import { ButtonComponent } from './button.component';
import { PendingComponent } from './pending/pending.component';
import { SoldComponent } from './sold/sold.component';
import { SoldConfirmedComponent } from './sold-confirmed/sold-confirmed.component';
import { SellingComponent } from './selling/selling.component';
import { PendingPaymentComponent } from './pending-payment/pending-payment.component';
import { PaymentSentComponent } from './payment-sent/payment-sent.component';
import { WaitingComponent } from './waiting/waiting.component';
import { CanceledComponent } from './canceled/canceled.component';
import { ReturningComponent } from './returning/returning.component';
import { InDeliveryComponent } from './in-delivery/in-delivery.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ButtonComponent,
    ResellComponent,
    PendingComponent,
    SoldComponent,
    SoldConfirmedComponent,
    SellingComponent,
    PendingPaymentComponent,
    PaymentSentComponent,
    WaitingComponent,
    CanceledComponent,
    ReturningComponent,
    InDeliveryComponent
  ],
  imports: [CommonModule, ButtonRoutingModule, TranslateModule]
})
export class ButtonModule {}
