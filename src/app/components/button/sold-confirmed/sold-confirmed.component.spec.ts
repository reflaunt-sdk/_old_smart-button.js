import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoldConfirmedComponent } from './sold-confirmed.component';

describe('SoldConfirmedComponent', () => {
  let component: SoldConfirmedComponent;
  let fixture: ComponentFixture<SoldConfirmedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoldConfirmedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoldConfirmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
