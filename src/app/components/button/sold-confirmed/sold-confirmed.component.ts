import { ButtonComponent } from '../button.component';
import { SMART_BUTTON_OPEN_APPLICATION_MESSAGE_TYPE } from '../../../iframe-message.const';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sold-confirmed',
  templateUrl: './sold-confirmed.component.html',
  styleUrls: ['./sold-confirmed.component.scss']
})
export class SoldConfirmedComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('sold-confirmed-open-button');
  }

  onClick() {
    this.sendOpenApplicationMessageToParent();
  }
}
