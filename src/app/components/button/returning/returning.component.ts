import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-returning',
  templateUrl: './returning.component.html',
  styleUrls: ['./returning.component.scss']
})
export class ReturningComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('returning-open-button');
  }
}
