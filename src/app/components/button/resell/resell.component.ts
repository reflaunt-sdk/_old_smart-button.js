import { SMART_BUTTON_OPEN_APPLICATION_MESSAGE_TYPE } from '../../../iframe-message.const';
import { Component, OnInit } from '@angular/core';
import { ButtonComponent } from '../button.component';

@Component({
  selector: 'app-resell',
  templateUrl: './resell.component.html',
  styleUrls: ['./resell.component.scss']
})
export class ResellComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('resell-open-button');
  }

  onClick() {
    this.sendOpenApplicationMessageToParent();
  }
}
