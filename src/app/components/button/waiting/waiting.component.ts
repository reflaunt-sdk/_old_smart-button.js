import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-waiting',
  templateUrl: './waiting.component.html',
  styleUrls: ['./waiting.component.scss']
})
export class WaitingComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('waiting-open-button');
  }
  onClick() {
    this.sendOpenApplicationMessageToParent();
  }
}
