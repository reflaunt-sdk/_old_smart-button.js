import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InDeliveryComponent } from './in-delivery.component';

describe('InDeliveryComponent', () => {
  let component: InDeliveryComponent;
  let fixture: ComponentFixture<InDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
