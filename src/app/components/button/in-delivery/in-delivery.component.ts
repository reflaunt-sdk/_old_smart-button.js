import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-in-delivery',
  templateUrl: './in-delivery.component.html',
  styleUrls: ['./in-delivery.component.scss']
})
export class InDeliveryComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('in-delivery-open-button');
  }
}
