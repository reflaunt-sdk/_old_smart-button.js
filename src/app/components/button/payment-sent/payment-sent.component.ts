import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-sent',
  templateUrl: './payment-sent.component.html',
  styleUrls: ['./payment-sent.component.scss']
})
export class PaymentSentComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('payment-sent-open-button');
  }
}
