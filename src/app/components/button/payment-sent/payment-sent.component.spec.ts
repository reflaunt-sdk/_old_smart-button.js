import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentSentComponent } from './payment-sent.component';

describe('PaymentSentComponent', () => {
  let component: PaymentSentComponent;
  let fixture: ComponentFixture<PaymentSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentSentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
