import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pending',
  templateUrl: './pending.component.html',
  styleUrls: ['./pending.component.scss']
})
export class PendingComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('pending-open-button');
  }
  onClick() {
    this.sendOpenApplicationMessageToParent();
  }
}
