import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResellComponent } from './resell/resell.component';
import { PendingComponent } from './pending/pending.component';
import { SoldComponent } from './sold/sold.component';
import { SoldConfirmedComponent } from './sold-confirmed/sold-confirmed.component';
import { SellingComponent } from './selling/selling.component';
import { PendingPaymentComponent } from './pending-payment/pending-payment.component';
import { PaymentSentComponent } from './payment-sent/payment-sent.component';
import { WaitingComponent } from './waiting/waiting.component';
import { CanceledComponent } from './canceled/canceled.component';
import { ReturningComponent } from './returning/returning.component';
import { InDeliveryComponent } from './in-delivery/in-delivery.component';

const appRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'resell',
        component: ResellComponent
      },
      {
        path: 'pending',
        component: PendingComponent
      },
      {
        path: 'sold',
        component: SoldComponent
      },
      {
        path: 'sold-confirmed',
        component: SoldConfirmedComponent
      },
      {
        path: 'selling',
        component: SellingComponent
      },
      {
        path: 'pending-payment',
        component: PendingPaymentComponent
      },
      {
        path: 'payment-sent',
        component: PaymentSentComponent
      },
      {
        path: 'waiting',
        component: WaitingComponent
      },
      {
        path: 'canceled',
        component: CanceledComponent
      },
      {
        path: 'returning',
        component: ReturningComponent
      },
      {
        path: 'returned',
        component: ResellComponent
      },
      {
        path: 'in-delivery',
        component: InDeliveryComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class ButtonRoutingModule {}
