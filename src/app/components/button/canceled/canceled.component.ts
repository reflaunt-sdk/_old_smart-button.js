import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-canceled',
  templateUrl: './canceled.component.html',
  styleUrls: ['./canceled.component.scss']
})
export class CanceledComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('canceled-open-button');
  }
}
