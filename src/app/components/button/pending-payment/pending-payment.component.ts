import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pending-payment',
  templateUrl: './pending-payment.component.html',
  styleUrls: ['./pending-payment.component.scss']
})
export class PendingPaymentComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('pending-payment-open-button');
  }
}
