import { ButtonComponent } from '../button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-selling',
  templateUrl: './selling.component.html',
  styleUrls: ['./selling.component.scss']
})
export class SellingComponent extends ButtonComponent implements OnInit {
  ngOnInit() {
    this.buttonOnInit('selling-open-button');
  }

  onClick() {
    this.sendOpenApplicationMessageToParent();
  }
}
