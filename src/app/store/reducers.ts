import * as _ from 'lodash';
import { combineReducers } from 'redux';
import {
  API_CALL_ERROR,
  API_CALL_SUCCESS,
  GET_PRODUCT_DATA_FROM_REFLAUNT_REQUEST,
  GET_PRODUCT_DATA_FROM_REFLAUNT_SUCCESS,
  GET_AVAILABLE_CONDITION_SUCCESS,
  ESTIMATE_PRICE_SUCCESS,
  GET_SMART_BUTTON_IFRAME_URL_SUCCESS,
  GET_BUTTON_ID_SUCCESS
} from './action';

const Root = (
  state = {
    buttonId: null,
    attributeFromClient: {},
    parentWindow: {
      index: 0,
      queryString: {
        open_smart_button_product_id: null,
        open_smart_button_order_id: null
      },
      path: ''
    },
    product: { fetched: false, item: {} },
    availableCondition: { fetched: false, items: [] },
    priceEstimated: { fetched: false, result: {} },
    smartObject: { fetched: false, item: {} }
  },
  action
) => {
  switch (action.type) {
    case GET_BUTTON_ID_SUCCESS: {
      const { id } = action.data;
      return _.assign({}, state, { buttonId: id });
    }
    case GET_PRODUCT_DATA_FROM_REFLAUNT_REQUEST: {
      const { params, index, queryString, path } = action.data.payload;
      return _.assign({}, state, {
        attributeFromClient: params,
        parentWindow: _.assign({}, state.parentWindow, { index, queryString, path })
      });
    }
    case GET_PRODUCT_DATA_FROM_REFLAUNT_SUCCESS: {
      return _.assign({}, state, { product: { fetched: true, item: _.head(action.data) } });
    }
    case GET_AVAILABLE_CONDITION_SUCCESS: {
      return _.assign({}, state, { availableCondition: { fetched: true, items: action.data } });
    }
    case ESTIMATE_PRICE_SUCCESS: {
      return _.assign({}, state, { priceEstimated: { fetched: true, result: action.data } });
    }
    case GET_SMART_BUTTON_IFRAME_URL_SUCCESS: {
      return _.assign({}, state, { smartObject: { fetched: true, item: action.data } });
    }
    default:
      return state;
  }
};

const Api = (state = { error: {}, success: {} }, action) => {
  switch (action.type) {
    case API_CALL_ERROR:
      return _.assign({}, state, { error: action.error.error });
    case API_CALL_SUCCESS:
      return _.assign({}, state, { success: action.success });
    default:
      return _.assign({}, state, { error: {}, success: {} });
  }
};

export default combineReducers({
  Root,
  Api
});
