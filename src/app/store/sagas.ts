import { takeEvery, fork, all, takeLatest, put, select } from 'redux-saga/effects';
import main from '../components/main.saga';
import {
  API_CALL_ERROR,
  GET_PRODUCT_DATA_FROM_REFLAUNT_REQUEST,
  GET_PRODUCT_DATA_FROM_REFLAUNT_SUCCESS,
  SMART_BUTTON_ESTIMATE_PRICE,
  GET_AVAILABLE_CONDITION_REQUEST,
  ESTIMATE_PRICE_REQUEST,
  GET_AVAILABLE_CONDITION_SUCCESS,
  ESTIMATE_PRICE_SUCCESS,
  RENDER_SMART_BUTTON,
  GET_SMART_BUTTON_IFRAME_URL_SUCCESS,
  GET_SMART_BUTTON_IFRAME_URL_REQUEST,
  SET_BUTTON_LANG
} from './action';
import { ApiService } from '../api/api.service';
import { AppInjector } from '../app-injector';
import { IframeService } from '../common/services/iframe.service';
import { SMART_BUTTON_GET_DATA_SUCCESS_MESSAGE_TYPE } from '../iframe-message.const';
import { transformAttributes, FALLBACK_LANG, AVAILABLE_LANGS } from '../app.const';
import { Router } from '@angular/router';
import {
  resellButtonRouter,
  pendingButtonRouter,
  soldButtonRouter,
  soldConfirmedButtonRouter,
  sellingButtonRouter,
  pendingPaymentButtonRouter,
  paymentSentButtonRouter,
  waitingButtonRouter,
  canceledButtonRouter,
  returningButtonRouter,
  returnedButtonRouter,
  inDeliveryButtonRouter
} from '../router.const';
import {
  STATUS_NEW,
  STATUS_PENDING,
  STATUS_SOLD,
  STATUS_SOLD_CONFIRMED,
  STATUS_SELLING,
  STATUS_PENDING_PAYMENT,
  STATUS_PAYMENT_SENT,
  STATUS_WAITING,
  STATUS_CANCELED,
  STATUS_RETURNING,
  STATUS_RETURNED,
  STATUS_IN_DELIVERY
} from '../models/Status';
import { TYPE_SMART_BUTTON } from '../models/SmartObject';
import { PriceFromClientService } from '../common/services/price-from-client.service';
import { TranslateService } from '@ngx-translate/core';

function* watchApiCallError() {
  yield takeEvery(API_CALL_ERROR, function* (action) {
    if ((action as any).error !== undefined) {
      if ((action as any).error.error !== undefined && (action as any).error.error.message !== undefined) {
        console.error((action as any).error);
        // alert((action as any).error.error.message);
        // Notification.show('warning', (action as any).error.error.message, 5000);
      }
    }
  });
}

function* watchGetProductDataFromReflauntRequest() {
  yield takeLatest(GET_PRODUCT_DATA_FROM_REFLAUNT_REQUEST, function* (action: any) {
    const api = AppInjector.get(ApiService);
    try {
      const { params } = action.data.payload;
      const data = {
        origin: window.location.origin,
        products: [
          {
            product_id: params.rf_product_id,
            order_id: params.rf_order_id,
            attributes: Object.assign({}, transformAttributes(params))
          }
        ]
      };
      const result = yield api.product.listBySmartButton(data).toPromise();
      yield put({ type: GET_PRODUCT_DATA_FROM_REFLAUNT_SUCCESS, data: result });
      yield put({ type: SET_BUTTON_LANG });
      yield put({ type: RENDER_SMART_BUTTON });
    } catch (e) {
      yield put({ type: API_CALL_ERROR, error: e });
    }
  });
}

function* watchSetButtonLang() {
  yield takeLatest(SET_BUTTON_LANG, function* (action: any) {
    const translate = AppInjector.get(TranslateService);
    const { attributeFromClient } = yield select((state: any) => state.Root);
    translate.use(
      AVAILABLE_LANGS.includes(attributeFromClient.rf_customer_language) ? attributeFromClient.rf_customer_language : FALLBACK_LANG
    );
  });
}

function* watchGetProductDataFromReflauntSuccess() {
  yield takeLatest(GET_PRODUCT_DATA_FROM_REFLAUNT_SUCCESS, function* (action: any) {
    const service = AppInjector.get(IframeService);
    const { data } = action;
    service.sendToParent({ type: SMART_BUTTON_GET_DATA_SUCCESS_MESSAGE_TYPE, data });
  });
}

function* watchRenderSmartButton() {
  yield takeLatest(RENDER_SMART_BUTTON, function* (action: any) {
    const router = AppInjector.get(Router);
    const { product, parentWindow } = yield select((state: any) => state.Root);
    switch (product.item.getStatusSlug()) {
      case STATUS_NEW: {
        router.navigate(resellButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_PENDING: {
        router.navigate(pendingButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_SOLD: {
        router.navigate(soldButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_SOLD_CONFIRMED: {
        router.navigate(soldConfirmedButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_SELLING: {
        router.navigate(sellingButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_PENDING_PAYMENT: {
        router.navigate(pendingPaymentButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_PAYMENT_SENT: {
        router.navigate(paymentSentButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_WAITING: {
        router.navigate(waitingButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_CANCELED: {
        router.navigate(canceledButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_RETURNING: {
        router.navigate(returningButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_RETURNED: {
        router.navigate(returnedButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      case STATUS_IN_DELIVERY: {
        router.navigate(inDeliveryButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
      default: {
        router.navigate(resellButtonRouter(), { queryParams: parentWindow.queryString });
        break;
      }
    }
  });
}

function* watchSmartButtonEstimatePrice() {
  yield takeLatest(SMART_BUTTON_ESTIMATE_PRICE, function* (action: any) {
    const { data } = action;
    yield put({
      type: GET_AVAILABLE_CONDITION_REQUEST,
      data,
      conditionData: { website: window.location.origin }
    });
  });
}

function* watchGetAvailableConditionRequest() {
  yield takeLatest(GET_AVAILABLE_CONDITION_REQUEST, function* (action: any) {
    const api = AppInjector.get(ApiService);
    try {
      const { data, conditionData } = action;
      const result = yield api.reflaunt.getAvailableCondition(conditionData).toPromise();
      yield put({ type: GET_AVAILABLE_CONDITION_SUCCESS, data: result });
      yield put({ type: ESTIMATE_PRICE_REQUEST, data, availableConditions: result });
    } catch (e) {
      yield put({ type: API_CALL_ERROR, error: e });
    }
  });
}

function* watchEstimateRequest() {
  yield takeLatest(ESTIMATE_PRICE_REQUEST, function* (action: any) {
    const api = AppInjector.get(ApiService);
    const rootState = yield select((state: any) => state.Root);
    const priceFromClientService = AppInjector.get(PriceFromClientService).make(rootState);
    try {
      let result: any = {};
      // switch (priceFromClientService.isUsingPriceFromClient()) {
      //   case true: {
      //     result = priceFromClientService.getResult();
      //     break;
      //   }
      //   case false: {
      const { data } = action;
      const requestPayload = {
        category: data.payload.params.rf_product_category,
        brand: data.payload.params.rf_product_designer,
        order_id: data.payload.params.rf_order_id,
        price: data.payload.params.rf_product_price,
        product_id: data.payload.params.rf_product_id,
        website: window.location.origin
      };
      try {
        result = yield api.reflaunt.smartPricer(requestPayload).toPromise();
        if (isNaN(result.max_price) || isNaN(result.middle_price) || isNaN(result.min_price)) {
          throw new Error();
        }
      } catch (e) {
        result = yield api.reflaunt.estimatePrice(requestPayload).toPromise();
      }
      const middle_price = result.middle_price;
      const min_price = (middle_price * (100 - Number(data.payload.params.rf_product_price_max))) / 100;
      const max_price = (middle_price * (100 + Number(data.payload.params.rf_product_price_min))) / 100;
      result = {
        min_price: min_price,
        middle_price: middle_price,
        max_price: max_price
      };
      //     break;
      //   }
      // }
      yield put({ type: ESTIMATE_PRICE_SUCCESS, data: result });
    } catch (e) {
      yield put({ type: API_CALL_ERROR, error: e });
    }
  });
}

function* watchGetSmartButtonIframeUrlRequest() {
  yield takeLatest(GET_SMART_BUTTON_IFRAME_URL_REQUEST, function* (action: any) {
    const api = AppInjector.get(ApiService);
    try {
      const { data } = action;
      const requestPayload = {
        client_key: data.payload.params.rf_reflaunt_client_key,
        smart_object_type: TYPE_SMART_BUTTON,
        test_application_iframe_src: data.payload.params.rf_test_application_iframe_src
      };
      const result = yield api.smartObject.getSmartObjectUrl(requestPayload).toPromise();
      yield put({ type: GET_SMART_BUTTON_IFRAME_URL_SUCCESS, data: result });
    } catch (e) {
      yield put({ type: API_CALL_ERROR, error: e });
    }
  });
}

export default function* sagas() {
  yield all(
    [
      ...main,
      watchGetProductDataFromReflauntRequest,
      watchGetProductDataFromReflauntSuccess,
      watchSmartButtonEstimatePrice,
      watchGetAvailableConditionRequest,
      watchEstimateRequest,
      watchRenderSmartButton,
      watchGetSmartButtonIframeUrlRequest,
      watchSetButtonLang,
      watchApiCallError
    ].map((item) => fork(item))
  );
}
