export const API_CALL_ERROR = 'API_CALL_ERROR';
export const API_CALL_SUCCESS = 'API_CALL_SUCCESS';

export const CLEAR_API_ERROR = 'CLEAR_API_ERROR';
export const CLEAR_API_SUCCESS = 'CLEAR_API_SUCCESS';
export const clearApiError = () => ({ type: CLEAR_API_ERROR });
export const clearApiSuccess = () => ({ type: CLEAR_API_SUCCESS });

export const GET_BUTTON_ID_SUCCESS = 'GET_BUTTON_ID_SUCCESS';

export const GET_PRODUCT_DATA_FROM_REFLAUNT_REQUEST = 'GET_PRODUCT_DATA_FROM_REFLAUNT_REQUEST';
export const GET_PRODUCT_DATA_FROM_REFLAUNT_SUCCESS = 'GET_PRODUCT_DATA_FROM_REFLAUNT_SUCCESS';

export const SMART_BUTTON_ESTIMATE_PRICE = 'SMART_BUTTON_ESTIMATE_PRICE';
export const GET_AVAILABLE_CONDITION_REQUEST = 'GET_AVAILABLE_CONDITION_REQUEST';
export const GET_AVAILABLE_CONDITION_SUCCESS = 'GET_AVAILABLE_CONDITION_SUCCESS';
export const ESTIMATE_PRICE_REQUEST = 'ESTIMATE_PRICE_REQUEST';
export const ESTIMATE_PRICE_SUCCESS = 'ESTIMATE_PRICE_SUCCESS';

export const RENDER_SMART_BUTTON = 'RENDER_SMART_BUTTON';

export const GET_SMART_BUTTON_IFRAME_URL_REQUEST = 'GET_SMART_BUTTON_IFRAME_URL_REQUEST';
export const GET_SMART_BUTTON_IFRAME_URL_SUCCESS = 'GET_SMART_BUTTON_IFRAME_URL_SUCCESS';

export const SET_BUTTON_LANG = 'SET_BUTTON_LANG';
