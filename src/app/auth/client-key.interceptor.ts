import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

const RF_RETAILER_CLIENT_KEY = 'Rf-Retailer-Public-Key';

@Injectable()
export class ClientKeyInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.indexOf(environment.apiUrl + '/api') > -1) {
      request = request.clone({
        setHeaders: {
          [RF_RETAILER_CLIENT_KEY]: environment.clientKey
        }
      });
    }

    // console.log(request, next);
    return next.handle(request);
  }
}
