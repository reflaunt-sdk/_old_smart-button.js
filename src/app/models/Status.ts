import Model from './Model';

class Status extends Model {
  constructor(options) {
    super();
    this.bind(options);
  }
}

export default Status;

export const STATUS_NEW = 'new';
export const STATUS_PENDING = 'pending';
export const STATUS_SOLD = 'sold';
export const STATUS_SOLD_CONFIRMED = 'sold-confirmed';
export const STATUS_SELLING = 'selling';
export const STATUS_PENDING_PAYMENT = 'pending-payment';
export const STATUS_PAYMENT_SENT = 'payment-sent';
export const STATUS_WAITING = 'waiting';
export const STATUS_CANCELED = 'canceled';
export const STATUS_RETURNING = 'returning';
export const STATUS_RETURNED = 'returned';
export const STATUS_IN_DELIVERY = 'in-delivery';
