import Model from './Model';

class SmartObject extends Model {
  constructor(options) {
    super();
    this.bind(options);
  }
}

export default SmartObject;

export const TYPE_SMART_BUTTON = 'smart-button';
