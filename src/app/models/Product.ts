import Model from './Model';
import ProductItem from './ProductItem';
import head from 'lodash/head';

class Product extends Model {
  constructor(options) {
    super();
    (this as any).product_items = (d) => d.data.map((item) => new ProductItem(item));
    this.bind(options);
  }

  getStatusSlug() {
    const { product_items } = this as any;
    if (product_items.length) {
      const item = head(product_items);
      return item.status.getSlug();
    }
    return null;
  }
}

export default Product;
