import Model from './Model';
import Status from './Status';

class ProductItem extends Model {
  constructor(options) {
    super();
    (this as any).status = (d) => new Status(d.data);
    this.bind(options);
  }
}

export default ProductItem;
