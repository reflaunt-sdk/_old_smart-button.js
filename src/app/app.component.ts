import { Component, OnInit, OnDestroy } from '@angular/core';
import { IframeService } from './common/services/iframe.service';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { BaseComponent } from './components/base.component';
import {
  GET_PRODUCT_DATA_FROM_REFLAUNT_REQUEST,
  SMART_BUTTON_ESTIMATE_PRICE,
  GET_SMART_BUTTON_IFRAME_URL_REQUEST,
  GET_BUTTON_ID_SUCCESS
} from './store/action';
import { INIT_PARENT_IFRAME_MESSAGE_TYPE, initIframeMessage, SMART_BUTTON_CLOSE_POPUP_MESSAGE_TYPE } from './iframe-message.const';
import { Subscription } from 'rxjs';
import { ApiService } from './api/api.service';
import { TrasnformDataFromParentWindowService } from './common/services/transform-data-from-parent-window.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseComponent implements OnInit, OnDestroy {
  reducer = 'Root';
  private iframeInitParentSubscription: Subscription;
  private iframeClosePopupSubscription: Subscription;

  constructor(
    private router: Router,
    private iframeService: IframeService,
    private api: ApiService,
    private transformer: TrasnformDataFromParentWindowService
  ) {
    super();
  }

  ngOnInit() {
    this.init();
    const {
      dispatchGetButtonIdSuccess,
      dispatchGetProductDataFromReflauntRequest,
      dispatchSmartButtonEstimatePrice,
      dispatchGetSmartButtonIframeUrl
    } = this as any;

    this.iframeInitParentSubscription = this.api.reflaunt.getButtonId().subscribe((result: any) => {
      dispatchGetButtonIdSuccess(result);
      this.iframeService.sendToParent(initIframeMessage(this.payload.buttonId));

      this.iframeService.message
        .pipe(
          filter((data: any) => {
            return data.type === INIT_PARENT_IFRAME_MESSAGE_TYPE && data.payload.id === this.payload.buttonId;
          })
        )
        .subscribe((data: any) => {
          data = this.transformer.transform(data);
          dispatchGetProductDataFromReflauntRequest(data);
          dispatchSmartButtonEstimatePrice(data);
          dispatchGetSmartButtonIframeUrl(data);
        });
    });

    this.iframeClosePopupSubscription = this.iframeService.message
      .pipe(
        filter((data: any) => {
          return data.type === SMART_BUTTON_CLOSE_POPUP_MESSAGE_TYPE;
        })
      )
      .subscribe((data: any) => {
        this.iframeService.sendToParent(initIframeMessage(this.payload.buttonId));
      });
  }

  ngOnDestroy() {
    this.iframeInitParentSubscription.unsubscribe();
    this.iframeClosePopupSubscription.unsubscribe();
  }

  mapStateToProps(state) {
    return {
      payload: state.Root
    };
  }

  mapDispatchToProps(dispatch) {
    return {
      dispatch,
      dispatchGetButtonIdSuccess: (data) => {
        dispatch({ type: GET_BUTTON_ID_SUCCESS, data });
      },
      dispatchGetProductDataFromReflauntRequest: (data) => {
        dispatch({ type: GET_PRODUCT_DATA_FROM_REFLAUNT_REQUEST, data });
      },
      dispatchSmartButtonEstimatePrice: (data) => {
        dispatch({ type: SMART_BUTTON_ESTIMATE_PRICE, data });
      },
      dispatchGetSmartButtonIframeUrl: (data) => {
        dispatch({ type: GET_SMART_BUTTON_IFRAME_URL_REQUEST, data });
      }
    };
  }
}
