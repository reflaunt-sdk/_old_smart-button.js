export const DATE_FORMAT_PATTERN = 'DD/MM/YYYY';

export const transformAttributes = (attribute) => {
  return Object.keys(attribute).reduce((acc, item) => {
    const key = item.replace('rf_', '').split('_').join('-');
    return Object.assign(acc, { [key]: attribute[item] });
  }, {});
};

export const AVAILABLE_LANGS = ['en', 'fr'];

export const FALLBACK_LANG = 'en';
