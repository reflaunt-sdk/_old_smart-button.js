import { ErrorHandler, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

export class AppErrorHandler implements ErrorHandler {
  isBrowser: boolean;
  constructor(@Inject(PLATFORM_ID) platformId: Object) {
    this.isBrowser = isPlatformBrowser(platformId);
  }
  handleError(error) {
    const message = (<Error>error).message;
    console.error(error);
    // if (this.isBrowser) {
    //   window.alert(message);
    // }
  }
}
