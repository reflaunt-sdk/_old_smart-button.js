import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

const PASSWORD_FORM_CONTROL = 'password';
const PASSWORD_CONFIRMATION_FORM_CONTROL = 'password_confirmation';
const CITY_OTHER = 'Others';
const PASSWORD_HAS_NUMBER_RULE_PATTERN = /\d/;
const PASSWORD_HAS_SPECIAL_CHARACTER_RULE_PATTERN = /(?=.*?[#?!@$%^&\*\-\=()+])/;
const PASSWORD_HAS_CAPITAL_LETTER_RULE_PATTERN = /(?=.*?[A-Z])/;
// tslint:disable-next-line: max-line-length
const VALID_EMAIL_RULE_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const patternValidator = (regex: RegExp, error: ValidationErrors): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: any } => {
    if (!control.value) {
      return null;
    }
    const valid = regex.test(control.value);
    return valid ? null : error;
  };
};

const cityValidator = (city, error: ValidationErrors): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: any } => {
    if (!control.value) {
      return null;
    }
    return control.value !== city ? null : error;
  };
};

export class CustomValidator {
  static passwordHasNumber(control: AbstractControl) {
    return patternValidator(PASSWORD_HAS_NUMBER_RULE_PATTERN, { hasNumber: true })(control);
  }

  static passwordHasSpecialChar(control: AbstractControl) {
    return patternValidator(PASSWORD_HAS_SPECIAL_CHARACTER_RULE_PATTERN, { hasSpecialChar: true })(control);
  }

  static passwordHasCapitalLetter(control: AbstractControl) {
    return patternValidator(PASSWORD_HAS_CAPITAL_LETTER_RULE_PATTERN, { hasCapitalLetter: true })(control);
  }

  static validEmail(control: AbstractControl) {
    return patternValidator(VALID_EMAIL_RULE_PATTERN, { validEmail: true })(control);
  }

  static validCity(control: AbstractControl) {
    return cityValidator(CITY_OTHER, { validCity: true })(control);
  }

  static passwordMatchValidator(control: AbstractControl) {
    const password: string = control.get(PASSWORD_FORM_CONTROL).value;
    const confirmPassword: string = control.get(PASSWORD_CONFIRMATION_FORM_CONTROL).value;
    return password === confirmPassword ? null : { noPasswordMatch: true };
  }
}
