import { FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import filter from 'lodash/filter';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'validation-error-message',
  templateUrl: './validation-error-message.component.html',
  styleUrls: ['./validation-error-message.component.scss']
})
export class ValidationErrorMessageComponent implements OnInit {
  @Input() rule: {
    control: FormControl;
    displayName: string;
    errors: Array<{ error: string; message: string }>;
  };

  public isOpen = true;
  public errors: Array<{ error: string; message: string }>;

  get errorItem() {
    return this.errors[0];
  }

  ngOnInit() {
    this.errors = Object.assign([], this.rule.errors);

    this.rule.control.valueChanges.subscribe((value) => {
      if (this.rule.control.errors) {
        const controlError = Object.keys(this.rule.control.errors);
        this.errors = filter(this.rule.errors, (item) => controlError.includes(item.error));
      }
    });
  }

  isDisplayErrorMessage() {
    if (this.rule.errors) {
      if (this.rule.errors.length && this.rule.control.touched) {
        return this.hasError();
      }
    }
    return false;
  }

  hasError() {
    return this.rule.control.hasError(this.errorItem.error);
  }

  onClose() {
    this.isOpen = false;
  }

  onOpen() {
    this.isOpen = true;
  }
}
