import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryPhoneCodeComponent } from './country-phone-code.component';

describe('ValidationPasswordRuleComponent', () => {
  let component: CountryPhoneCodeComponent;
  let fixture: ComponentFixture<CountryPhoneCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CountryPhoneCodeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryPhoneCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
