import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Countries } from './countries';

export const DISPLAY_ERROR = 'error';
export const DISPLAY_SUCCESS = 'success';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'country-phone-code',
  templateUrl: './country-phone-code.component.html',
  styleUrls: ['./country-phone-code.component.scss']
})
export class CountryPhoneCodeComponent implements OnInit {
  @Output() valueChange: EventEmitter<any> = new EventEmitter();

  public item;
  public COUNTRIES = Countries;
  public country: any;
  public form: any;
  constructor() {}

  ngOnInit() {}
  valueChanged(e) {
    this.valueChange.emit(e);
  }
}
