import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiSuccessMessageComponent } from './api-success-message.component';

describe('ApiSuccessMessageComponent', () => {
  let component: ApiSuccessMessageComponent;
  let fixture: ComponentFixture<ApiSuccessMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiSuccessMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiSuccessMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
