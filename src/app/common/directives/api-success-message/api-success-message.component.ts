import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import isEmpty from 'lodash/isEmpty';
import hasIn from 'lodash/hasIn';
@Component({
  selector: 'api-success-message',
  templateUrl: './api-success-message.component.html',
  styleUrls: ['./api-success-message.component.scss']
})
export class ApiSuccessMessageComponent implements OnInit {

  @Input() success: {
    success_code: number;
    message: string;
  };

  @Output() closed = new EventEmitter<void>();

  private isOpen = false;
  private isEmpty = isEmpty;
  private hasIn = hasIn;

  constructor() {
  }

  ngOnInit() {}

  isDisplayMessage() {
    !isEmpty(this.success) && hasIn(this.success, 'message') ? (this.isOpen = true) : (this.isOpen = false);
    return this.isOpen;
  }

  onClose() {
    this.isOpen = false;
    this.closed.emit();
  }

  onOpen() {
    this.isOpen = true;
  }

}
