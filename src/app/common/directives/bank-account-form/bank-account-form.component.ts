import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Countries } from '../country-phone-code/countries';
import { ApiService } from '../../../api/api.service';
import { BaseComponent } from '../../../components/base.component';
import * as _ from 'lodash';
import { clearApiError, clearApiSuccess } from '../../../store/action';
import { Observable } from 'rxjs';
import * as $ from 'jquery';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'bank-account-form',
  templateUrl: './bank-account-form.component.html',
  styleUrls: ['./bank-account-form.component.scss']
})
export class BankAccountFormComponent extends BaseComponent implements OnInit {
  @Input() submitText: String;
  @Input() data: Observable<any>;
  @Input() isProcessing?: boolean;
  @Input() isDisabled?: boolean;
  @Input() title?: String;

  @Output() submit = new EventEmitter<any>();

  @ViewChild('currencyError', { static: true }) currencyError: ElementRef;
  // @ViewChild('zipcodeError', { static: true }) zipcodeError: ElementRef;
  @ViewChild('accountNumberError', { static: true }) accountNumberError: ElementRef;
  @ViewChild('ibanNumberError', { static: true }) ibanNumberError: ElementRef;
  @ViewChild('sortCodeError', { static: true }) sortCodeError: ElementRef;
  // @ViewChild('countryError', { static: true }) countryError: ElementRef;
  // @ViewChild('cityError', { static: true }) cityError: ElementRef;
  // @ViewChild('routerNumberError', { static: true }) routerNumberError: ElementRef;

  public addBankAccountForm = this.fb.group({
    // city: ['', Validators.required],
    // zip_code: ['', Validators.required],
    // country: ['', Validators.required],
    currency: ['', Validators.required],
    // routing_number: ['', Validators.required],
    iban_number: [''],
    account_number: [''],
    sort_code: ['']
  });
  public isSubmit = false;

  public zipcodeValidationRule = {
    control: this.zipcode,
    displayName: 'Zip code',
    errors: [
      {
        error: 'required',
        message: 'is required'
      }
    ]
  };
  public countryValidationRule = {
    control: this.country,
    displayName: 'Country',
    errors: [
      {
        error: 'required',
        message: 'is required'
      }
    ]
  };
  public ibanNumberValidationRule = {
    control: this.ibanNumber,
    displayName: 'IBAN number',
    errors: [
      {
        error: 'required',
        message: 'is required'
      }
    ]
  };
  public sortCodeValidationRule = {
    control: this.sortCode,
    displayName: 'Sort code',
    errors: [
      {
        error: 'required',
        message: 'is required'
      }
    ]
  };
  public currencyValidationRule = {
    control: this.currency,
    displayName: 'Currency',
    errors: [
      {
        error: 'required',
        message: 'is required'
      }
    ]
  };
  public accountNumberValidationRule = {
    control: this.accountNumber,
    displayName: 'Account number',
    errors: [
      {
        error: 'required',
        message: 'is required'
      }
    ]
  };
  public cityValidationRule = {
    control: this.city,
    displayName: 'Town or city',
    errors: [
      {
        error: 'required',
        message: 'is required'
      }
    ]
  };
  // public routerNumberValidationRule = {
  //   control: this.routerNumber,
  //   displayName: 'Router number',
  //   errors: [
  //     {
  //       error: 'required',
  //       message: 'is required'
  //     }
  //   ]
  // };

  public currencies = ['EUR', 'GBP'];

  public countries = Countries;

  public pickEur = false;
  public pickGbp = false;
  public edit = false;

  constructor(private fb: FormBuilder, private api: ApiService) {
    super();
  }

  ngOnInit() {
    this.init();
    this.data.subscribe((result: any) => {
      if (!_.isEmpty(result)) {
        // this.addBankAccountForm.controls['city'].setValue(result.city);
        // this.addBankAccountForm.controls['zip_code'].setValue(result.zip_code);
        this.addBankAccountForm.controls['currency'].setValue(result.currency);
        // this.addBankAccountForm.controls['country'].setValue(result.country);
        if (result.currency === 'GBP') {
          this.pickGbp = true;
          this.pickEur = false;
          this.addBankAccountForm.get('iban_number').disable();
          this.addBankAccountForm.get('account_number').enable();
          this.addBankAccountForm.get('sort_code').enable();
          this.addBankAccountForm.controls['account_number'].setValue(result.account_number);
          this.addBankAccountForm.controls['sort_code'].setValue(result.bank_code);
        } else {
          this.pickEur = true;
          this.pickGbp = false;
          this.addBankAccountForm.get('iban_number').enable();
          this.addBankAccountForm.get('account_number').disable();
          this.addBankAccountForm.get('sort_code').disable();
          this.addBankAccountForm.controls['iban_number'].setValue(result.account_number);
        }
      }
    });
  }

  get zipcode() {
    return this.addBankAccountForm.get('zip_code');
  }
  get country() {
    return this.addBankAccountForm.get('country');
  }
  get ibanNumber() {
    return this.addBankAccountForm.get('iban_number');
  }
  get accountNumber() {
    return this.addBankAccountForm.get('account_number');
  }
  get sortCode() {
    return this.addBankAccountForm.get('sort_code');
  }
  get currency() {
    return this.addBankAccountForm.get('currency');
  }
  get city() {
    return this.addBankAccountForm.get('city');
  }
  // get routerNumber() {
  //   return this.addBankAccountForm.get('routing_number');
  // }
  get apiError() {
    return this.store.getState().Api.error;
  }

  get apiSuccess() {
    return this.store.getState().Api.success;
  }

  isDisableButton() {
    return this.isDisabled || false;
  }

  getTitleContent() {
    return this.title;
  }

  changeCurrencies(e) {
    if (e === 'EUR') {
      this.pickEur = true;
      this.pickGbp = false;
      this.addBankAccountForm.get('iban_number').enable();
      this.addBankAccountForm.get('account_number').disable();
      this.addBankAccountForm.get('sort_code').disable();
    }
    if (e === 'GBP') {
      this.pickGbp = true;
      this.pickEur = false;
      this.addBankAccountForm.get('iban_number').disable();
      this.addBankAccountForm.get('account_number').enable();
      this.addBankAccountForm.get('sort_code').enable();
    }
  }

  isDisplayProcessing() {
    return this.isProcessing || false;
  }

  clearInput(value) {
    this.addBankAccountForm.get(`${value}`).setValue('');
  }

  onCloseApiErrorMessage() {
    const { dispatchClearApiError } = this as any;
    dispatchClearApiError();
  }

  onCloseApiSuccessMessage() {
    const { dispatchClearApiSuccess } = this as any;
    dispatchClearApiSuccess();
  }

  onSubmitHandler() {
    this.addBankAccountForm.markAllAsTouched();
    // if (this.country.invalid) {
    //   (this.countryError as any).onOpen();
    // }
    // if (this.zipcode.invalid) {
    //   (this.zipcodeError as any).onOpen();
    // }
    if (this.sortCode.invalid) {
      (this.sortCodeError as any).onOpen();
    }
    if (this.ibanNumber.invalid) {
      (this.ibanNumberError as any).onOpen();
    }
    if (this.accountNumber.invalid) {
      (this.accountNumberError as any).onOpen();
    }
    if (this.currency.invalid) {
      (this.currencyError as any).onOpen();
    }
    // if (this.city.invalid) {
    //   (this.cityError as any).onOpen();
    // }
    // if (this.routerNumber.invalid) {
    //   (this.routerNumberError as any).onOpen();
    // }
    if (this.addBankAccountForm.valid) {
      if (this.addBankAccountForm.value.iban_number) {
        this.addBankAccountForm.value.account_number = this.addBankAccountForm.value.iban_number;
      }
      if (this.addBankAccountForm.value.sort_code) {
        this.addBankAccountForm.value.bank_code = this.addBankAccountForm.value.sort_code;
      }
      this.submit.emit(this.addBankAccountForm);
    }
  }

  mapStateToProps(state) {
    return {
      payload: state
    };
  }

  mapDispatchToProps(dispatch) {
    return {
      dispatch,
      dispatchClearApiError: () => {
        dispatch(clearApiError());
      },
      dispatchClearApiSuccess: () => {
        dispatch(clearApiSuccess());
      }
    };
  }
}
