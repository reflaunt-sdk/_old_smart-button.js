import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import isEmpty from 'lodash/isEmpty';
import hasIn from 'lodash/hasIn';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'api-error-message',
  templateUrl: './api-error-message.component.html',
  styleUrls: ['./api-error-message.component.scss']
})
export class ApiErrorMessageComponent implements OnInit {
  @Input() error: {
    error_code: number;
    message: string;
  };

  @Output() closed = new EventEmitter<void>();

  private isOpen = false;
  private isEmpty = isEmpty;
  private hasIn = hasIn;

  constructor() {}

  ngOnInit() {}

  isDisplayMessage() {
    !isEmpty(this.error) && hasIn(this.error, 'message') ? (this.isOpen = true) : (this.isOpen = false);
    return this.isOpen;
  }

  onClose() {
    this.isOpen = false;
    this.closed.emit();
  }

  onOpen() {
    this.isOpen = true;
  }
}
