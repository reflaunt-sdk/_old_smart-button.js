import { RouterModule } from '@angular/router';
import { PipesModule } from '../pipes/pipes.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationComponent } from './pagination/pagination.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchInputComponent } from './search-input/search-input.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { LengthAwarePaginatorComponent } from './length-aware-paginator/length-aware-paginator.component';
import { SortByFieldComponent } from './sort-by-field/sort-by-field.component';
import { PerPageComponent } from './per-page/per-page.component';
import { LoaderComponent } from './loader/loader.component';
import { ValidationErrorMessageComponent } from './validation-error-message/validation-error-message.component';
import { ApiErrorMessageComponent } from './api-error-message/api-error-message.component';
import { ValidationPasswordRuleComponent } from './validation-password-rule/validation-password-rule.component';
import { ApiSuccessMessageComponent } from './api-success-message/api-success-message.component';
import { CountryPhoneCodeComponent } from './country-phone-code/country-phone-code.component';
import { BankAccountFormComponent } from './bank-account-form/bank-account-form.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, PipesModule],
  exports: [
    PaginationComponent,
    SearchInputComponent,
    SearchFormComponent,
    LengthAwarePaginatorComponent,
    SortByFieldComponent,
    PerPageComponent,
    LoaderComponent,
    ValidationErrorMessageComponent,
    ApiErrorMessageComponent,
    ValidationPasswordRuleComponent,
    ApiSuccessMessageComponent,
    CountryPhoneCodeComponent,
    BankAccountFormComponent
  ],
  declarations: [
    PaginationComponent,
    SearchInputComponent,
    SearchFormComponent,
    LengthAwarePaginatorComponent,
    SortByFieldComponent,
    PerPageComponent,
    LoaderComponent,
    ValidationErrorMessageComponent,
    ApiErrorMessageComponent,
    ValidationPasswordRuleComponent,
    ApiSuccessMessageComponent,
    CountryPhoneCodeComponent,
    BankAccountFormComponent
  ]
})
export class DirectivesModule {}
