import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationPasswordRuleComponent } from './validation-password-rule.component';

describe('ValidationPasswordRuleComponent', () => {
  let component: ValidationPasswordRuleComponent;
  let fixture: ComponentFixture<ValidationPasswordRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidationPasswordRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationPasswordRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
