import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import filter from 'lodash/filter';

export const DISPLAY_ERROR = 'error';
export const DISPLAY_SUCCESS = 'success';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'validation-password-rule',
  templateUrl: './validation-password-rule.component.html',
  styleUrls: ['./validation-password-rule.component.scss']
})
export class ValidationPasswordRuleComponent implements OnInit {
  @Input() display: string;
  @Input() rule: {
    control: FormControl;
    displayName?: string;
    successMessage?: string;
    errors?: Array<{ error: string; message: string }>;
  };

  public DISPLAY_ERROR = DISPLAY_ERROR;
  public DISPLAY_SUCCESS = DISPLAY_SUCCESS;

  public isOpen = true;
  public errors: Array<{ error: string; message: string }>;

  constructor() {}

  get errorItem() {
    return this.errors[0];
  }

  ngOnInit() {
    this.errors = Object.assign([], this.rule.errors);

    this.rule.control.valueChanges.subscribe((value) => {
      if (this.rule.control.errors) {
        const controlError = Object.keys(this.rule.control.errors);
        this.errors = filter(this.rule.errors, (item) => controlError.includes(item.error));
      }
    });
  }

  isDisplayErrorMessage() {
    if (this.rule.errors) {
      if (this.rule.errors.length && this.rule.control.touched) {
        return this.hasError();
      }
    }
    return false;
  }

  hasError() {
    if (this.errorItem) {
      return this.rule.control.hasError(this.errorItem.error);
    }
    return false;
  }

  onClose() {
    this.isOpen = false;
  }

  onOpen() {
    this.isOpen = true;
  }
}
