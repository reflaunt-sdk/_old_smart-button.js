import { fromEvent } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class IframeService {
  private event = 'message';

  constructor() {}

  get rawMessage() {
    return fromEvent(window, this.event);
  }

  get message() {
    return fromEvent(window, this.event).pipe(map((message: any) => message.data));
  }

  sendToParent(data) {
    window.parent.postMessage(data, '*');
  }

  sendToChild(iframe, data) {
    iframe.contentWindow.postMessage(data, '*');
  }
}
