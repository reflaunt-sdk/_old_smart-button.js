import { ClearApiErrorMessageService } from './clear-api-error-message.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IframeService } from './iframe.service';
import { PriceFromClientService } from './price-from-client.service';
import { TrasnformDataFromParentWindowService } from './transform-data-from-parent-window.service';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [ClearApiErrorMessageService, IframeService, PriceFromClientService, TrasnformDataFromParentWindowService]
})
export class ServicesModule {}
