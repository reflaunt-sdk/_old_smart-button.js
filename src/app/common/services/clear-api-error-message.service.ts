import { clearApiError } from '../../store/action';
import { Store } from '../../store/store.module';
import { UrlTree, CanActivateChild, CanActivate } from '@angular/router';
import { RouterStateSnapshot } from '@angular/router';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class ClearApiErrorMessageService implements CanActivate, CanActivateChild {
  constructor(private store: Store) {}

  clearErrorMessage(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree {
    const { dispatch } = this.store.store as any;
    dispatch(clearApiError());
    return true;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree {
    return this.clearErrorMessage(next, state);
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree {
    return this.clearErrorMessage(next, state);
  }
}
