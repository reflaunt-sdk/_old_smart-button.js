import { Injectable } from '@angular/core';
import hasIn from 'lodash/hasIn';

@Injectable()
export class PriceFromClientService {
  private state;

  make(state) {
    this.state = state;
    return this;
  }

  get attributeFromClient() {
    return this.state.attributeFromClient;
  }

  isUsingPriceFromClient() {
    if (hasIn(this.attributeFromClient, 'rf_product_price_max') && hasIn(this.attributeFromClient, 'rf_product_price_min')) {
      return true;
    }
    return false;
  }

  getResult() {
    const original_price = Number(this.attributeFromClient.rf_product_price);
    const min_price = (original_price * (100 - Number(this.attributeFromClient.rf_product_price_max))) / 100;
    const max_price = (original_price * (100 - Number(this.attributeFromClient.rf_product_price_min))) / 100;
    const middle_price = (min_price + max_price) / 2;
    return {
      min_price: min_price,
      middle_price: Math.ceil(middle_price),
      max_price: max_price
    };
  }
}
