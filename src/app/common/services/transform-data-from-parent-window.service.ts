import { fromEvent } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class TrasnformDataFromParentWindowService {
  transform(data) {
    return Object.assign(data, {
      payload: Object.assign(data.payload, {
        params: Object.assign(data.payload.params, {
          rf_customer_language: this.transformLanguage(data.payload.params.rf_customer_language),
          rf_product_gender: this.transformGender(data.payload.params.rf_product_gender),
          rf_product_name: this.transformName(data.payload.params.rf_product_name)
        })
      })
    });
  }

  transformName(name) {
    return name.replace(/[\[\]]/g, '');
  }

  transformLanguage(language) {
    return language.split('_')[0].toLowerCase();
  }

  transformGender(gender) {
    return ((str) => {
      const data = str.toLowerCase();
      switch (str.toLowerCase()) {
        case 'women':
          return 'female';
        case 'men':
          return 'male';
        default:
          return data;
      }
    })(gender);
  }
}
