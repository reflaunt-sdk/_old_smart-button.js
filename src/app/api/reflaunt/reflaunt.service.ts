import { BaseService } from '../base.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { tap, map, catchError } from 'rxjs/operators';

@Injectable()
export class ReflauntService extends BaseService {
  public url = '/api/v1/reflaunt';

  getAvailableCondition(params: {}): Observable<any> {
    const queryParams = new HttpParams({ fromObject: params });
    return this.http.get(this.apiUrl.getApiUrl(`${this.url}/price/estimate/available-conditions`), { params: queryParams }).pipe(
      tap((result) => {}),
      map((result: any) => result.data),
      catchError((error) => {
        throw error;
      })
    );
  }

  estimatePrice(params): Observable<any> {
    return this.http.post(this.apiUrl.getApiUrl(`${this.url}/price/estimate`), { ...params, ...{ designer: params.designer } }).pipe(
      tap((result) => {}),
      map((result: any) => result.data),
      catchError((error) => {
        throw error;
      })
    );
  }

  smartPricer(params): Observable<any> {
    const payload = {
      marketplace: 'styletribute',
      marketplace_group: 'all',
      condition: 'excellent',
      product_in_cluster_count_threshold: 5,
      mode: 'standard',
      model_product_in_cluster_count_threshold: '2'
    };
    return this.http
      .post(this.apiUrl.getApiUrl(`${this.url}/smart-pricer`), { ...payload, ...params, ...{ original_price_to_predict: params.price } })
      .pipe(
        tap((result) => {}),
        map((result: any) => {
          const { data } = result;
          return {
            min_price: Math.ceil(data.recommended_price_low),
            middle_price: Math.ceil(data.recommended_price),
            max_price: Math.ceil(data.recommended_price_high)
          };
        }),
        catchError((error) => {
          throw error;
        })
      );
  }

  getButtonId(): Observable<any> {
    return this.http.post(this.apiUrl.getApiUrl(`${this.url}/smart-objects/object-id`)).pipe(
      tap((result) => {}),
      map((result: any) => result),
      catchError((error) => {
        throw error;
      })
    );
  }
}
