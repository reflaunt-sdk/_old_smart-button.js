import { PermissionService } from './permission/permission.service';
import { Injectable } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { RoleService } from './role/role.service';
import { FileService } from './file/file.service';
import { UserService } from './user/user.service';
import { ProductService } from './product/product.service';
import { ReflauntService } from './reflaunt/reflaunt.service';
import { SmartObjectService } from './smart-object/smart-object.service';
@Injectable()
export class ApiService {
  constructor(
    public auth: AuthService,
    public role: RoleService,
    public permission: PermissionService,
    public file: FileService,
    public user: UserService,
    public product: ProductService,
    public reflaunt: ReflauntService,
    public smartObject: SmartObjectService
  ) {}
}
