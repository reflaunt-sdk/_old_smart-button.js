import { PermissionService } from './permission/permission.service';
import { RoleService } from './role/role.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from './api.service';
import { AuthService } from './auth/auth.service';
import { ApiUrl } from './api-url.service';
import { FileService } from './file/file.service';
import { UserService } from './user/user.service';
import { ProductService } from './product/product.service';
import { ReflauntService } from './reflaunt/reflaunt.service';
import { SmartObjectService } from './smart-object/smart-object.service';
@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    ApiUrl,
    ApiService,
    AuthService,
    RoleService,
    PermissionService,
    FileService,
    UserService,
    ProductService,
    ReflauntService,
    SmartObjectService
  ]
})
export class ApiModule {}
