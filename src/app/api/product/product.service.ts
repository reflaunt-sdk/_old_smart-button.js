import { BaseService } from '../base.service';
import Product from '../../models/Product';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';

@Injectable()
export class ProductService extends BaseService {
  public url = '/api/v1/products';
  public model = Product;

  listBySmartButton(data: { origin: string; products: Array<any> }): Observable<any> {
    return this.http.post(this.apiUrl.getApiUrl(`${this.url}/list/smart-button`), data).pipe(
      tap((result) => {}),
      map((result: any) => result.data.map((item) => new this.model(item))),
      catchError((error) => {
        throw error;
      })
    );
  }
}
