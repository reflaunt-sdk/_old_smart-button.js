import { BaseService } from '../base.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import SmartObject, { TYPE_SMART_BUTTON } from '../../models/SmartObject';
import hasIn from 'lodash/hasIn';

@Injectable()
export class SmartObjectService extends BaseService {
  public url = '/api/v1/smart-object';

  getSmartObjectUrl(data): Observable<any> {
    if (hasIn(data, 'test_application_iframe_src')) {
      if (data.test_application_iframe_src) {
        const item = new SmartObject({
          type: TYPE_SMART_BUTTON,
          url: data.test_application_iframe_src
        });
        return of(item);
      }
    }
    return this.http.post(this.apiUrl.getApiUrl(`${this.url}/get-smart-object-url`), data).pipe(
      tap((result) => {}),
      map((result: any) => new SmartObject(result.data)),
      catchError((error) => {
        throw error;
      })
    );
  }
}
