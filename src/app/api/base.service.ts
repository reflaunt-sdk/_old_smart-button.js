import { ApiUrl } from './api-url.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import * as _ from 'lodash';
import LengthAwarePaginator from '../models/LengthAwarePaginator';
import { AppInjector } from '../app-injector';

export class BaseService {
  public url = '';
  public model;
  public http;
  public apiUrl;
  public preloader;

  constructor() {
    this.http = AppInjector.get(HttpClient);
    this.apiUrl = AppInjector.get(ApiUrl);
  }
  /**
   * Get list of all resource with pagination
   *
   * @param Object Optinal
   *
   * @return Observable
   */
  get(params: {}): Observable<any> {
    const queryParams = new HttpParams({ fromObject: params });
    return this.http.get(this.apiUrl.getApiUrl(this.url), { params: queryParams }).pipe(
      tap((result) => {
      }),
      map((result) =>
        _.assign(
          {},
          {
            items: (result as any).data.map((item) => new this.model(item)),
            pagination: new LengthAwarePaginator((result as any).meta.pagination)
          }
        )
      ),
      catchError((error) => {
        throw error;
      })
    );
  }

  /**
   * Get list of all resource
   *
   * @param Object Optional
   *
   * @return Observable
   */
  list(params = {}): Observable<any> {
    return this.http.post(this.apiUrl.getApiUrl(`${this.url}/list`), params).pipe(
      tap((result) => {
      }),
      map((result) => _.map((result as any).data, (item) => new this.model(item))),
      catchError((error) => {
        throw error;
      })
    );
  }

  /**
   * Update resource by given id
   *
   * @param Int Resource ID
   * @param Object Resource Data
   *
   * @return Observable
   */
  update(id, data): Observable<any> {
    return this.http.put(this.apiUrl.getApiUrl(this.url) + '/' + id, data).pipe(
      tap((result) => {
      }),
      map((result) => new this.model((result as any).data)),
      catchError((error) => {
        throw error;
      })
    );
  }

  /**
   * Delete resource by given id
   *
   * @param Int Resource ID
   *
   * @return Observable
   */
  delete(id): Observable<any> {
    return this.http.delete(this.apiUrl.getApiUrl(this.url) + '/' + id).pipe(
      tap((result) => {
      }),
      catchError((error) => {
        throw error;
      })
    );
  }

  create(data): Observable<any> {
    return this.http.post(this.apiUrl.getApiUrl(this.url), data).pipe(
      tap((result) => {
      }),
      map((result) => new this.model((result as any).data)),
      catchError((error) => {
        throw error;
      })
    );
  }

  /**
   * Get resource by given id
   *
   * @param Int Resource ID
   * @param Object Optional
   *
   * @return Observable
   */
  show(id, params?): Observable<any> {
    params = params || {};
    const queryParams = new HttpParams({ fromObject: params });
    return this.http.get(this.apiUrl.getApiUrl(this.url) + '/' + id, { params: queryParams }).pipe(
      tap((result) => {
      }),
      map((result) => new this.model((result as any).data)),
      catchError((error) => {
        throw error;
      })
    );
  }
}
